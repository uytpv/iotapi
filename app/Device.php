<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Device extends Model {
	protected $table = "devices";
	protected $primaryKey = 'id';
	public $timestamp = true;
	protected $fillable = [
		'device_name',
		'device_id',
		'user_id',
	];
}
