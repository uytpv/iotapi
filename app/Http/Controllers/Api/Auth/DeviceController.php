<?php

namespace App\Http\Controllers\Api\Auth;

use App\Device;
use App\Http\Controllers\Controller;
use Bluerhinos\phpMQTT;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

/**
 * Created by PhpStorm.
 * User: traphucvinhuy
 * Date: 3/15/18
 * Time: 23:06
 */
class DeviceController extends Controller {
	public $server = '';
	public $port = '';
	public $username = '';
	public $password = '';

	public function __construct() {
		$this->server = env( 'MQTT_SERVER_IP' );
		$this->port   = env( 'MQTT_SERVER_PORT' );
	}

	public function turnTheLight( Request $request ) {
		$action    = $request->route( 'action' );
		$device    = Device::find( $request->route( 'id' ) );
		$client_id = $device->device_id;
		$mqtt      = new phpMQTT( $this->server, $this->port, $client_id );
		if ( $mqtt->connect( true, null, $this->username, $this->password ) ) {
			$mqtt->publish( '/turn/light', $action, 0 );
			$mqtt->close();
			$device->status = $action;
			$device->save();

			return response()->json( [
				'statusCode' => 1,
				'message'    => 'Success',
			] );
		} else {
			return response()->json( [ 'message' => 'Error!!' ], 204 );
		}
	}

	public function getLightState( Request $request ) {
		$device    = Device::find( $request->route( 'id' ) );
		$client_id = $device->device_id;
		$mqtt      = new phpMQTT( $this->server, $this->port, $client_id );
		if ( ! $mqtt->connect( true, null, $this->username, $this->password ) ) {
			exit( 1 );
		}
		$topics['/state/light'] = array( "qos" => 0, "function" => "procmsg" );
		$mqtt->subscribe( $topics, 0 );
		while ( $mqtt->proc() ) {
		}
		$mqtt->close();
		function procmsg( $topic, $msg ) {
			echo "Msg Recieved: " . date( "r" ) . "\n";
			echo "Topic: {$topic}\n\n";
			echo "\t$msg\n\n";
		}
	}

	private function getClientId() {
		return 'esp8266';
	}

	public function devices( Request $request ) {
		$userId  = $request->route( 'userId' );
		$devices = Device::where( 'user_id', '=', $userId )->get();

		return response()->json( [
			'statusCode' => 1,
			'message'    => 'Get My Devices',
			'devices'    => $devices
		] );
	}

	public function addDevice( Request $request ) {
		$this->validate( $request, [
			'device_name' => 'required|min:3',
			'device_id'   => 'required|unique:devices',
		] );
		$device              = new Device();
		$device->device_name = $request->get( 'device_name' );
		$device->device_id   = $request->get( 'device_id' );
		$device->user_id     = $request->get( 'user_id' );
		$device->status      = 0;
		$device->created_at  = Carbon::now();
		$device->updated_at  = Carbon::now();
		$device->save();

		return response()->json( [ 'message' => 'Add device successful' ], 200 );
	}
}