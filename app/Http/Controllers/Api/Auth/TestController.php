<?php

namespace App\Http\Controllers\Api\Auth;

use Bluerhinos\phpMQTT;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;


class TestController extends Controller {
	public function test( Request $request ) {
		$action = $request->route( 'action' );
		var_dump( $action );
		$server    = '192.168.0.101';     // change if necessary
		$port      = 1883;                     // change if necessary
		$username  = '';                   // set your username
		$password  = '';                   // set your password
		$client_id = 'light_control'; // make sure this is unique for connecting to sever - you could use uniqid()
		$mqtt      = new phpMQTT( $server, $port, $client_id );

		if ( $mqtt->connect( true, null, $username, $password ) ) {

			$mqtt->publish( '/test/light1', $action, 0 );
			$mqtt->close();
		} else {
			echo 'Time out!\n';
		}
	}
}
